emailValidator(val) {
  return !val.contains('@') ? 'Invalid UserId' : null;
}

passwordValidator(val) {
  return val.length < 4 ? 'Password too short' : null;
}

requiredValidator(val) {
  return val.length < 1 ? 'required' : null;
}
