import 'dart:async';
import 'dart:convert';
import '../environment.dart';
import 'package:http/http.dart' as http;

class UserServices {
  Future signup(userDetail) async {
    var _reqbody = userDetail.toJson();
    print('reqSignup$_reqbody');
    final response = await http.post(
        Uri.parse(Environment().baseURL + 'api/v1/users/register'),
        headers: {'Accept': 'application/json'},
        body: _reqbody);

    if (response.statusCode == 201) {
      final body = json.decode(response.body);
      print('Success ${body['message']}');
      return body['message'];
    } else {
      print('OOps ${response.body}');
      throw Exception(response.body);
    }
  }

  login(userDetail) {
    var _reqbody = userDetail.toJson();
    return http
        .post(Uri.parse(Environment().baseURL + 'api/v1/users/authenticate'),
            headers: {'Accept': 'application/json'}, body: _reqbody)
        .then((value) {
      if (value.statusCode == 200) {
        final res = json.decode(value.body);
        return res;
      } else {
        return json.decode(value.body);
      }
    });
  }

  otp(otpDetail) {
    print('before sending$otpDetail');
    // otpDetail['_id'] = otpDetail['id'];
    var _reqbody = {'id': otpDetail['id'], 'otp': otpDetail['otp']};
    //   print('afterConvertion$_reqbody');
    return http
        .post(Uri.parse(Environment().baseURL + 'api/v1/users/otpValidate'),
            headers: {'Accept': 'application/json'}, body: _reqbody)
        .then((value) {
      if (value.statusCode == 200) {
        final res = json.decode(value.body);
        return res;
      } else {
        return json.decode(value.body);
      }
    });
  }
}
