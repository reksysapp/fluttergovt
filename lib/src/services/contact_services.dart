import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import '../environment.dart';

class ContactServices {
  late List contacts;
  late String _status;

  Future districtJson() async {
    var data = await rootBundle.loadString('assets/district.json');
    print('loadjson${data.runtimeType}');
    var _resData = await json.decode(data);
    print('afterDecode${_resData.runtimeType}');
    // print("asdfas$_contacts");

    return _resData;
  }

  Future talukJson(dist) async {
    var data = await rootBundle.loadString('assets/taluk.json');
    // print('taluk${dist}');
    var _resData = await json.decode(data);
    // print('afterDecode${_resData[0]['id']}');

    var _response =
        _resData.singleWhere((data) => data['id'] == dist, orElse: () => null);

    if (_response != null) {
      return _response['Taluk'];
    } else {
      return _response;
    }
  }

  Future stateJson() async {
    var data = await rootBundle.loadString('assets/state.json');
    print('loadjson${data.runtimeType}');
    var _resData = await json.decode(data);
    print('afterDecode${_resData.runtimeType}');
    // print("asdfas$_contacts");

    return _resData;
  }

  Future villageJson(taluk) async {
    var data = await rootBundle.loadString('assets/village.json');
    print('loadjson${data.runtimeType}');
    var _resData = await json.decode(data);
    print('afterDecode${_resData.runtimeType}');
    var _response =
        _resData.firstWhere((data) => data['id'] == taluk, orElse: () => null);

    if (_response != null) {
      return _response['Villages'];
    } else {
      return _response;
    }
  }

  getData() {
    print(Environment().baseURL + 'api/v1/cust/list');
    return http.post(Uri.parse(Environment().baseURL + 'api/v1/cust/list'),
        headers: {'Accept': 'application/json'}).then((value) {
      if (value.statusCode == 200) {
        final res = json.decode(value.body);
        print("afterdecode${res.runtimeType}");
        return res;
      } else {
        return json.decode(value.body);
      }
    });
  }

  updateRecord(record) {
    print('update record$record');
    var _reqbody = record;
    print('beforeConvertion${_reqbody.runtimeType}');
    return http
        .put(Uri.parse(Environment().baseURL + 'api/v1/cust/'),
            headers: {'Accept': 'application/json'}, body: _reqbody)
        .then((value) {
      if (value.statusCode == 200) {
        final res = json.decode(value.body);
        return res;
      } else {
        return json.decode(value.body);
      }
    });
  }

  uploadData(custDetail) {
    // print();
    var _reqbody = {};
    _reqbody['cust'] = json.encode(custDetail);
    // _reqbody['_id'] = _reqbody['id'];
    print('reqbody$_reqbody');
    print(Environment().baseURL + 'api/v1/users/authenticate');
    return http
        .post(Uri.parse(Environment().baseURL + 'api/v1/cust/'),
            headers: {'Accept': 'application/json'}, body: _reqbody)
        .then((value) {
      if (value.statusCode == 200) {
        final res = json.decode(value.body);
        return res;
      } else {
        return json.decode(value.body);
      }
    });
  }

  checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      _status = "Mobile";
      // I am connected to a mobile network.
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.
      _status = "Wifi";
    } else {
      _status = "No Network";
    }
    return _status;
  }
  // return http.get(Environment().baseURL + 'users').then((value) {
  //   if (value.statusCode == 200) {
  //     final res = json.decode(value.body);

  //     return res;
  //   } else {
  //     print('OOps ${value.body}');
  //     throw Exception(value.body);
  //   }
  // });

}
