class UserModel {
  UserModel();

  String id = "";
  String user_role = "";
  String password = "";
  String user_group = "";
  String branch = "";
  String mobile = "";
  String name = "";
  String otp = "";
  String email_id = "";
  String created_at = "";

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

UserModel _$UserFromJson(Map<String, dynamic> json) {
  return UserModel()
    ..id = json['id'] as String
    ..user_role = json['user_role'] as String
    ..password = json['password'] as String
    ..user_group = json['user_group'] as String
    ..branch = json['branch'] as String
    ..otp = json['otp'] as String
    ..mobile = json['mobile'] == null ? "" : json['mobile'] as String
    ..name = json['name'] as String
    ..email_id = json['email_id'] as String
    ..created_at = json['created_at'] as String;
}

Map<String, dynamic> _$UserToJson(UserModel instance) => <String, dynamic>{
      'id': instance.id,
      'user_role': instance.user_role,
      'password': instance.password,
      'user_group': instance.user_group,
      'branch': instance.branch,
      'mobile': instance.mobile,
      'otp': instance.otp,
      'name': instance.name,
      'email_id': instance.email_id,
      'created_at': instance.created_at
    };
