class ContactModel {
  ContactModel();
  String _id = "";
  String custid = "";
  String pic = "";
  String district = "";
  String state = "";
  String taluk = "";
  String village = "";
  String custName = "";
  String dueDate = "";
  String address = "";
  String area = "";
  String paidOn = "";
  String pincode = "";
  String dueAmt = "";
  String paidAmt = "";
  String balAmt = "";
  String status = "";
  String geoLocation = "";

  factory ContactModel.fromJson(Map<String, dynamic> json) =>
      _$ContactModelFromJson(json);
  Map<String, dynamic> toJson() => _$ContactModelToJson(this);
}

ContactModel _$ContactModelFromJson(Map<String, dynamic> json) {
  return ContactModel()
    .._id = json['_id'] as String
    ..custid = json['custid'] as String
    ..pic = json['pic'] as String
    ..custName = json['custName'] as String
    ..state = json['state'] as String
    ..district = json['district'] as String
    ..taluk = json['taluk'] as String
    ..village = json['village'] as String
    ..dueDate = json['dueDate'] as String
    ..address = json['address'] as String
    ..area = json['area'] as String
    ..pincode = json['pincode'] as String
    ..paidOn = json['paidOn'] as String
    ..paidAmt = json['paidAmt'] as String
    ..dueAmt = json['dueAmt'] as String
    ..status = json['status'] as String
    ..balAmt = json['balAmt'] as String
    ..geoLocation = json['geoLocation'] as String;
}

Map<String, dynamic> _$ContactModelToJson(ContactModel instance) =>
    <String, dynamic>{
      '_id': instance._id,
      'custid': instance.custid,
      'pic': instance.pic,
      'state': instance.state,
      'district': instance.district,
      'taluk': instance.taluk,
      'village': instance.village,
      'custName': instance.custName,
      'dueDate': instance.dueDate,
      'address': instance.address,
      'area': instance.area,
      'pincode': instance.pincode,
      'paidOn': instance.paidOn,
      'paidAmt': instance.paidAmt,
      'dueAmt': instance.dueAmt,
      'status': instance.status,
      'balAmt': instance.balAmt,
      'geoLocation': instance.geoLocation
    };
