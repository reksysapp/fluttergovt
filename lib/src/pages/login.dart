import 'package:flutter/material.dart';
import 'package:fluttergovt/src/components/constants.dart';
import 'package:fluttergovt/src/components/loader.dart';
import 'package:fluttergovt/src/models/user_model.dart';
import 'package:fluttergovt/src/pages/list_view.dart';
import 'package:fluttergovt/src/pages/otp.dart';
import 'package:fluttergovt/src/pages/sign_up.dart';
import 'package:fluttergovt/src/services/user_services.dart';
import 'package:fluttergovt/src/validators/validations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:local_auth/local_auth.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);
  static const routeName = '/';
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var _user = UserModel();
  bool loader = false;
  @override
  void initState() {
    super.initState();
  }

  final scaffoldkey = GlobalKey<ScaffoldState>();
  final formkey = GlobalKey<FormState>();
  void _submit() {
    final form = formkey.currentState;
    form!.save();
    // Navigator.restorablePushNamed(context, Otp.routeName);
    performLogin();
    // if (form.validate()) {
    //   form.save();
    //   performLogin();
    // }
  }

  void performLogin() {
    // Loader().openLoadingDialog(context);
    // Navigator.pushReplacementNamed(context, '/otp');
    // Navigator.pushReplacementNamed(context, '/otp');
    // setState(() {
    //   loader = true;
    // });
    print("asdfasdf${_user.id}");

    UserServices logser = UserServices();
    var result = logser.login(_user);

    result.then((u) async {
      Navigator.pop(context);
      // setState(() {
      //   loader = true;
      // });
      if (u['code'] == 'SUCCESS') {
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('userId', u['_id']);
        Navigator.restorablePushNamed(context, Otp.routeName);
      } else {
        final snackbar = SnackBar(
          content: Text(u['message']),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackbar);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final ButtonStyle style = ElevatedButton.styleFrom(
        textStyle: const TextStyle(fontSize: 20, color: Color(0xff476cfb)));

    final LocalAuthentication _localAuthentication = LocalAuthentication();
    String _message = "Not Authorized";

    Future<void> _authenticateMe() async {
// 8. this method opens a dialog for fingerprint authentication.
//    we do not need to create a dialog nut it popsup from device natively.
      bool authenticated = false;
      try {
        authenticated = await _localAuthentication.authenticate(
            localizedReason: "Authenticate for Testing", // message for dialog
            useErrorDialogs: true, // show error in dialog
            stickyAuth: true, // native process
            biometricOnly: true);
        setState(() {
          _message = authenticated ? "Authorized" : "Not Authorized";
          if (_message == 'Authorized') {
            Navigator.restorablePushNamed(context, ListViewPage.routeName);
          } else {
            final snackbar = new SnackBar(
              content: new Text(_message),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackbar);
          }
        });
      } catch (e) {
        print(e);
      }
      if (!mounted) return;
    }

    return Scaffold(
      key: scaffoldkey,
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // (loader == true) ? Loader() : Text(""),
              Text(
                "LOGIN",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: size.height * 0.03),
              Padding(
                padding: EdgeInsets.all(30.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(143, 148, 251, .2),
                                blurRadius: 20.0,
                                offset: Offset(0, 10))
                          ]),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey[100]!))),
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Id",
                                    hintStyle:
                                        TextStyle(color: Colors.grey[400])),
                                // validator: (val) => emailValidator(val),
                                onSaved: (val) => _user.id = val!,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(8.0),
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Password",
                                    hintStyle:
                                        TextStyle(color: Colors.grey[400])),
                                validator: (val) => passwordValidator(val),
                                onSaved: (val) => _user.password = val!,
                                obscureText: true,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                style: style,
                onPressed: () => {_submit()},
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 16.0),
                ),
              ),
              ElevatedButton(
                style: style,
                onPressed: _authenticateMe,
                child: Text(
                  'FingerPrint',
                  style: TextStyle(color: Colors.white, fontSize: 16.0),
                ),
              ),
              // RoundedButton(
              //   text: "LOGIN",

              //   press: () {
              //     _submit();
              //   },
              // ),
              SizedBox(height: size.height * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Don’t have an Account ? ",
                    style: TextStyle(color: kPrimaryColor),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return SignUp();
                          },
                        ),
                      );
                    },
                    child: Text(
                      "Sign Up",
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
