import 'package:flutter/material.dart';
import 'package:fluttergovt/src/services/contact_services.dart';
import 'package:fluttergovt/src/settings/settings_view.dart';

import 'details_view.dart';

class ListViewPage extends StatefulWidget {
  ListViewPage({Key? key}) : super(key: key);
  static const routeName = '/listViewPage';

  @override
  State<ListViewPage> createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
  var _contacts;

  @override
  void initState() {
    super.initState();
    // _download();
  }

  Future<void> _download() async {
    print("get from api");
    var res = await ContactServices().getData();
    print('aftergetdata${res.length}');
    _contacts = res;
    // for (var i = 0; i < res.length; i++) {
    //   _contacts = ContactModel.fromJson(res[i]);

    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sample Items'),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.restorablePushNamed(context, SettingsView.routeName);
            },
          ),
        ],
      ),

      // To work with lists that may contain a large number of items, it’s best
      // to use the ListView.builder constructor.
      //
      // In contrast to the default ListView constructor, which requires
      // building all Widgets up front, the ListView.builder constructor lazily
      // builds Widgets as they’re scrolled into view.
      body: FutureBuilder(
          future: ContactServices().getData(),
          builder: (context, snapshot) {
            _contacts = snapshot.data;
            return snapshot.hasData
                ? ListView.builder(
                    itemCount: _contacts.length,
                    itemBuilder: (context, i) {
                      return Column(
                        children: <Widget>[
                          const Divider(height: 0.1),
                          GestureDetector(
                              child: ListTile(
                                leading: CircleAvatar(
                                  backgroundColor: Colors.grey,
                                  backgroundImage:
                                      NetworkImage(_contacts[i]['pic']),
                                ),
                                title: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      _contacts[i]['custName'],
                                      style: const TextStyle(
                                          fontSize: 13.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    // new Text(
                                    //   contacts[i]['time'],
                                    //   style: TextStyle(fontSize: 11.0, color: Colors.grey),
                                    // )
                                  ],
                                ),
                                // subtitle: Text(
                                //   _contacts[i].scheme,
                                //   style:
                                //       TextStyle(color: Colors.grey, fontSize: 11.0),
                                // ),
                              ),
                              onTap: () => {
                                    Navigator.restorablePushNamed(
                                        context, Details.routeName,
                                        arguments: _contacts[i]),
                                    print(
                                      _contacts[i]['address'],
                                      // Navigator.push(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //     builder: (context) => PaymentDetails(
                                      //       custRecord: _contacts[i],
                                      //     ),
                                      //   ),
                                    )
                                  }) // {Navigator.pushReplacementNamed(context, '/payment')})
                        ],
                      );
                    })
                : Center(child: CircularProgressIndicator());
          }),
    );
  }
}
