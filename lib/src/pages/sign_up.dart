import 'package:flutter/material.dart';
import 'package:fluttergovt/src/components/constants.dart';
import 'package:fluttergovt/src/models/user_model.dart';
import 'package:fluttergovt/src/pages/login.dart';
import 'package:fluttergovt/src/services/user_services.dart';
import 'package:fluttergovt/src/validators/validations.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);
  static const routeName = '/signup';
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  var _user = UserModel();

  final scaffoldkey = GlobalKey<ScaffoldState>();
  final formkey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final ButtonStyle style = ElevatedButton.styleFrom(
        textStyle: const TextStyle(fontSize: 20, color: Color(0xff476cfb)));
    return Scaffold(
      key: scaffoldkey,
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "SIGNUP",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: size.height * 0.03),
              Padding(
                padding: EdgeInsets.all(30.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(143, 148, 251, .2),
                                blurRadius: 20.0,
                                offset: Offset(0, 10))
                          ]),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8.0),
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Name",
                                    hintStyle:
                                        TextStyle(color: Colors.grey[400])),
                                onSaved: (val) => _user.name = val!,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(8.0),
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Mobile No.",
                                    hintStyle:
                                        TextStyle(color: Colors.grey[400])),
                                // validator: (val) => emailValidator(val),
                                onSaved: (val) => _user.email_id = val!,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(8.0),
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Password",
                                    hintStyle:
                                        TextStyle(color: Colors.grey[400])),
                                validator: (val) => passwordValidator(val),
                                onSaved: (val) => _user.password = val!,
                                obscureText: true,
                              ),
                            ),
                            // Container(
                            //   padding: EdgeInsets.all(8.0),
                            //   child: TextFormField(
                            //     decoration: InputDecoration(
                            //         border: InputBorder.none,
                            //         hintText: "OTP",
                            //         hintStyle:
                            //             TextStyle(color: Colors.grey[400])),
                            //     // validator: (val) => emailValidator(val),
                            //     onSaved: (val) => _user.otp = val,
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                style: style,
                onPressed: () => {_submit()},
                child: Text(
                  'Sign Up',
                  style: TextStyle(color: Colors.white, fontSize: 16.0),
                ),
              ),
              // RoundedButton(
              //   text: "SIGNUP",
              //   press: () {
              //     _submit();
              //   },
              // ),
              SizedBox(height: size.height * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text(
                    "Already have an Account ? ",
                    style: TextStyle(color: kPrimaryColor),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return const Login();
                          },
                        ),
                      );
                    },
                    child: const Text(
                      "Sign In",
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  _submit() {
    final form = formkey.currentState;
    if (form!.validate()) {
      form.save();
      UserServices().signup((_user));

      snackDisplay("successfully created");
    }
  }

  Future snackDisplay(res) async {
    final SnackBar snackbar = SnackBar(
      content: Text(res),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
    Navigator.pushReplacementNamed(context, '/login');
  }
}
