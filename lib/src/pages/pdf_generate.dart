import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

import 'list_view.dart';
import 'package:permission_handler/permission_handler.dart';

class PdfGenerate extends StatefulWidget {
  const PdfGenerate({Key? key}) : super(key: key);
  static const routeName = '/pdfgen';

  @override
  _PdfGenerateState createState() => _PdfGenerateState();
}

class _PdfGenerateState extends State<PdfGenerate> {
  final pdf = pw.Document();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    writeOnPdf();
    savePdf();
  }

  saveLogic() async {
    Directory? directory = await getExternalStorageDirectory();
    String fileName = "xyz.pdf";
    String newPath = "";
    print(directory);
    List<String> paths = directory!.path.split("/");
    for (int x = 1; x < paths.length; x++) {
      String folder = paths[x];
      if (folder != "Android") {
        newPath += "/" + folder;
      } else {
        break;
      }
    }
    newPath = newPath + "/generatePdf";
    directory = Directory(newPath);

    if (!await directory.exists()) {
      await directory.create(recursive: true);
    }
    if (await directory.exists()) {
      final File file = File(directory.path + "/$fileName");
      // your logic for saving the file.
      file.writeAsBytesSync(await pdf.save());
      Navigator.restorablePushNamed(context, ListViewPage.routeName);
    }
  }

  writeOnPdf() async {
    final font = await rootBundle.load('assets/HindMadurai-Regular.ttf');

    final ttf = pw.TtfFont(font);

    pdf.addPage(
      pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        margin: pw.EdgeInsets.all(32),
        footer: _buildFooter,
        build: (pw.Context context) {
          return <pw.Widget>[
            pw.Header(
                level: 0,
                child: pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                    children: <pw.Widget>[
                      pw.Text('Geo-Coordinate Document on Property',
                          textScaleFactor: 0.5),
                    ])),
            pw.Header(level: 1, text: 'What is Lorem Ipsum?'),

            // Write All the paragraph in one line.
            // For clear understanding
            // here there are line breaks.
            pw.Paragraph(
                text: 'வணக்கம் என்பது தமிழ் கலாச்சாரத்தில் மரபாகும்.',
                // ignore: prefer_const_constructors
                style: pw.TextStyle(font: ttf, fontSize: 12)),
            pw.Paragraph(
                text:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod'
                    'tempor incididunt ut labore et dolore magna aliqua. Nunc mi ipsum faucibus'
                    'vitae aliquet nec. Nibh cras pulvinar mattis nunc sed blandit libero'
                    'volutpat. Vitae elementum curabitur vitae nunc sed velit. Nibh tellus'
                    'molestie nunc non blandit massa. Bibendum enim facilisis gravida neque.'
                    'Arcu cursus euismod quis viverra nibh cras pulvinar mattis. Enim diam'
                    'vulputate ut pharetra sit. Tellus pellentesque eu tincidunt tortor'
                    'aliquam nulla facilisi cras fermentum. '),
            pw.Header(level: 1, text: 'This is Header'),
            pw.Paragraph(
                text:
                    'tempor incididunt ut labore et dolore magna aliqua. Nunc mi ipsum faucibus'
                    'vitae aliquet nec. Nibh cras pulvinar mattis nunc sed blandit libero'
                    'volutpat. Vitae elementum curabitur vitae nunc sed velit. Nibh tellus'
                    'molestie nunc non blandit massa. Bibendum enim facilisis gravida neque.'
                    'Arcu cursus euismod quis viverra nibh cras pulvinar mattis. Enim diam'
                    'vulputate ut pharetra sit. Tellus pellentesque eu tincidunt tortor'
                    'aliquam nulla facilisi cras fermentum. '),
            pw.Paragraph(
                text:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod'
                    'tempor incididunt ut labore et dolore magna aliqua. Nunc mi ipsum faucibus'
                    'vitae aliquet nec. Nibh cras pulvinar mattis nunc sed blandit libero'
                    'volutpat. Vitae elementum curabitur vitae nunc sed velit. Nibh tellus'
                    'molestie nunc non blandit massa. Bibendum enim facilisis gravida neque.'
                    'Arcu cursus euismod quis viverra nibh cras pulvinar mattis. Enim diam'
                    'vulputate ut pharetra sit. Tellus pellentesque eu tincidunt tortor'
                    'aliquam nulla facilisi cras fermentum. '),
            pw.Padding(padding: const pw.EdgeInsets.all(10)),
            pw.Table.fromTextArray(context: context, data: const <List<String>>[
              <String>['Year', 'Sample'],
              <String>['SN0', 'GFG1'],
              <String>['SN1', 'GFG2'],
              <String>['SN2', 'GFG3'],
              <String>['SN3', 'GFG4'],
            ]),

            pw.BarcodeWidget(
              height: 70, width: 70,
              // color: PdfColor.fromHex("#000000"),
              barcode: pw.Barcode.qrCode(),
              data: "My data",
            ),
          ];
        },
      ),
    );
    Directory? directory = await getExternalStorageDirectory();
    String fileName = "xyz6.pdf";
    String newPath = "";
    print(directory);
    List<String> paths = directory!.path.split("/");
    for (int x = 1; x < paths.length; x++) {
      String folder = paths[x];
      if (folder != "Android") {
        newPath += "/" + folder;
      } else {
        break;
      }
    }
    newPath = newPath + "/generatePdf";
    directory = Directory(newPath);

    if (!await directory.exists()) {
      await directory.create(recursive: true);
    }
    if (await directory.exists()) {
      final File file = File(directory.path + "/$fileName");
      var status = await Permission.storage.status;
      print('status$status');
      if (await Permission.storage.request().isGranted) {
        // Either the permission was already granted before or the user just granted it.
        file.writeAsBytesSync(await pdf.save());
        Navigator.restorablePushNamed(context, ListViewPage.routeName);
      }
    }
  }

  pw.Widget _buildFooter(pw.Context context) {
    return pw.Row(
      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
      crossAxisAlignment: pw.CrossAxisAlignment.end,
      children: [
        // pw.Container(
        //   height: 20,
        //   width: 100,
        //   child: pw.BarcodeWidget(
        //     barcode: pw.Barcode.pdf417(),
        //     data: 'Invoice# 123',
        //     drawText: false,
        //   ),
        // ),
        pw.Text(
          'Page ${context.pageNumber}/${context.pagesCount}',
          style: const pw.TextStyle(
            fontSize: 12,
            color: PdfColors.black,
          ),
        ),
      ],
    );
  }

  Future savePdf() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String documentPath = documentDirectory.path;
    print(documentPath);
    File file = File("$documentPath/example.pdf");
    file.writeAsBytesSync(await pdf.save());
    Navigator.restorablePushNamed(context, ListViewPage.routeName);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: ElevatedButton(
          onPressed: () async => {await writeOnPdf(), savePdf()},
          child: const Text(
            'என்பது',
            style: TextStyle(
                color: Colors.white, fontFamily: 'tamil', fontSize: 16.0),
          ),
        ),
      ),
    );
  }
}
