import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fluttergovt/src/services/contact_services.dart';

class Details extends StatefulWidget {
  Details({
    Key? key,
  }) : super(key: key);

  static const routeName = '/details';
  @override
  _DetailsState createState() => _DetailsState();
  var custRecord;
}

class _DetailsState extends State<Details> {
  late Position _position;
  var _custRecord;
  List districtList = [];
  List talukList = [];
  List villageList = [];
  List stateList = [];

  // _DetailsState(this._custRecord);

  final formkey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    // _custRecord.status == "" ? _custRecord.geoLocation = "" : null;
    WidgetsBinding.instance!.addPostFrameCallback((_) => {
          _getStateList(),
          _getDistrictList(),
          _getTalukList(_custRecord['district']),
          _getVillageList(_custRecord['taluk'])
          // if (_custRecord.status != "")
          //   {

          //   }
        });
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Container();
  // }
  @override
  Widget build(BuildContext context) {
    _custRecord = ModalRoute.of(context)!.settings.arguments as Map;
    print("custRecord${_custRecord['state']}");
    final ButtonStyle style = ElevatedButton.styleFrom(
        textStyle: const TextStyle(fontSize: 10, color: Color(0xff476cfb)));
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Payment Details"),
        ),
        key: scaffoldkey,
        body: (_custRecord != null)
            ? ListView(
                padding: const EdgeInsets.all(10.0),
                children: <Widget>[
                  Form(
                    key: formkey,
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            const Expanded(
                              flex: 1,
                              child: Text(
                                'State',
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    value: _custRecord["state"],
                                    iconSize: 30,
                                    icon: (null),
                                    style: const TextStyle(
                                      color: Colors.black54,
                                      fontSize: 16,
                                    ),
                                    hint: Text('Select State'),
                                    onChanged: (_custRecord["status"] == "")
                                        ? (newValue) {
                                            // setState(() {
                                            //   _custRecord.state = newValue;
                                            //   print(_custRecord.state);
                                            // });
                                          }
                                        : null,
                                    items: stateList.map((item) {
                                      print(item['name']);
                                      return DropdownMenuItem(
                                        child: Text(item['name']),
                                        value: item['name'].toString(),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            const Expanded(
                              flex: 1,
                              child: Text(
                                'District',
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    value: _custRecord['district'],
                                    iconSize: 30,
                                    icon: (null),
                                    style: const TextStyle(
                                      color: Colors.black54,
                                      fontSize: 16,
                                    ),
                                    hint: const Text('Select District'),
                                    disabledHint: Text(_custRecord['district']),
                                    onChanged: (_custRecord['status'] == "")
                                        ? (newValue) {
                                            setState(() {
                                              _custRecord['district'] =
                                                  newValue;
                                              _custRecord['taluk'] = "";
                                              _custRecord['village'] = "";
                                              talukList = [];
                                              villageList = [];

                                              _getTalukList(newValue);
                                              print("talukList$talukList");
                                            });
                                          }
                                        : null,
                                    items: districtList.map((item) {
                                      print(item['name']);
                                      return DropdownMenuItem(
                                        child: Text(item['name']),
                                        value: item['name'].toString(),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),

                            // Expanded(flex: 3, child: Text(_custRecord.scheme))
                          ],
                        ),
                        const SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            const Expanded(
                              flex: 1,
                              child: Text(
                                'Taluk',
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    value: _custRecord['taluk'],
                                    iconSize: 30,
                                    icon: (null),
                                    style: const TextStyle(
                                      color: Colors.black54,
                                      fontSize: 16,
                                    ),
                                    hint: const Text('Select Taluk'),
                                    onChanged: (_custRecord['status'] == "")
                                        ? (newValue) {
                                            setState(() {
                                              _custRecord['taluk'] = newValue;
                                              _custRecord['village'] = "";
                                              villageList = [];
                                              _getVillageList(newValue);
                                            });
                                          }
                                        : null,
                                    items: talukList.map((item) {
                                      print(item['name']);
                                      return DropdownMenuItem(
                                        child: Text(item['name']),
                                        value: item['name'].toString(),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Text(
                                'Village',
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    value: _custRecord['village'],
                                    iconSize: 30,
                                    icon: (null),
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 16,
                                    ),
                                    hint: Text('Select Village'),
                                    onChanged: (_custRecord['status'] == "")
                                        ? (newValue) {
                                            setState(() {
                                              _custRecord['village'] = newValue;
                                              _getVillageList(newValue);
                                              print(_custRecord['village']);
                                            });
                                          }
                                        : null,
                                    items: villageList.map((item) {
                                      return DropdownMenuItem(
                                        child: Text(item['name']),
                                        value: item['name'].toString(),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Divider(height: 20.0, thickness: 2.0),
                        Row(
                          children: <Widget>[
                            Text(
                              'Pay',
                              style: TextStyle(fontSize: 18.0),
                            ),
                            SizedBox(
                              width: 100.0,
                            ),
                            Container(
                              width: 150.0,
                              child: (_custRecord['status'] != "")
                                  ? Text(_custRecord['paidAmt'])
                                  : TextFormField(
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          contentPadding:
                                              const EdgeInsets.all(8),
                                          border: OutlineInputBorder(),
                                          hintText: "Amount",
                                          hintStyle: TextStyle(
                                              color: Colors.grey[400])),
                                      onChanged: (val) => {
                                            print('value$val'),
                                            _custRecord['paidAmt'] = val,
                                          }),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 2,
                              child: Text(
                                'GeoLocation',
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Expanded(
                                flex: 2,
                                child: Text(_custRecord['geoLocation'])),
                            (_custRecord['status'] == "")
                                ? Expanded(
                                    flex: 1,
                                    child: ElevatedButton(
                                      style: style,
                                      onPressed: () => {
                                        getlocation(),
                                      },
                                      child: const Text(
                                        'Fetch',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14.0),
                                      ),
                                    ),
                                  )
                                : const Text(""),
                          ],
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 2,
                              child: Text(
                                'Status',
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Expanded(
                                flex: 3, child: Text(_custRecord['status']))
                          ],
                        ),
                        SizedBox(
                          height: 50.0,
                        ),
                        (_custRecord['status'] != "")
                            ? Row(
                                // crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  RaisedButton(
                                    color: Color(0xff476cfb),
                                    onPressed: () {
                                      Navigator.pop(context);
                                      // Navigator.pushReplacementNamed(
                                      //     context, '/home');
                                    },
                                    elevation: 4.0,
                                    splashColor: Colors.blueGrey,
                                    child: Text(
                                      'Close',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16.0),
                                    ),
                                  ),
                                ],
                              )
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  RaisedButton(
                                    color: Color(0xff476cfb),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    elevation: 4.0,
                                    splashColor: Colors.blueGrey,
                                    child: Text(
                                      'Cancel',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16.0),
                                    ),
                                  ),
                                  ElevatedButton(
                                    style: style,
                                    onPressed: () => {
                                      _add(context),
                                    },
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16.0),
                                    ),
                                  ),
                                  // RaisedButton(
                                  //   color: Color(0xff476cfb),
                                  //   onPressed: () => {
                                  //     _add(context),
                                  //   },
                                  //   elevation: 4.0,
                                  //   splashColor: Colors.blueGrey,
                                  //   child: Text(
                                  //     'Submit',
                                  //     style: TextStyle(
                                  //         color: Colors.white, fontSize: 16.0),
                                  //   ),
                                  // )
                                ],
                              ),
                      ],
                    ),
                  ),
                ],
              )
            : Text("Loading"),
      ),
    );
  }

  _add(BuildContext context) {
    print("submit");
    final form = formkey.currentState;
    form!.save();

    ContactServices().checkConnectivity().then((res) async => {
          if (res == "No Network")
            {
              // await helper.updateNote(_custRecord),
            }
          // Navigator.pop(context)}
          else
            {
              // await helper.updateNote(_custRecord),
              ContactServices().updateRecord(_custRecord),
              Navigator.pop(context)
            }
        });
    setState(() {});
  }

  getlocation() async {
    // Loader().openLoadingDialog(context);
    LocationPermission permission;
    // setState(() {
    //   _custRecord.geoLocation = _position.toString();
    // });
    // Test if location services are enabled.
    // serviceEnabled = await Geolocator.isLocationServiceEnabled();
    // print(serviceEnabled);
    // if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    // return Future.error('Location services are disabled.');
    // permission = await Geolocator.requestPermission();
    // }
    // print(permission);
    permission = await Geolocator.checkPermission();
    print(permission);
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    if (permission == LocationPermission.whileInUse ||
        permission == LocationPermission.always) {
      _position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
    }
    // LocationPermission permission = await Geolocator.checkPermission();
    // print("permission");
    // if (permission == LocationPermission.always) {
    //   _position =
    //       await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    // } else {
    //   LocationPermission reqpermission = await requestPermission();
    //   if (reqpermission == LocationPermission.always) {
    //     _position =
    //         await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    //   }
    // }
    setState(() {
      _custRecord['geoLocation'] = _position.toString();
      _custRecord = _custRecord;
      // Navigator.pop(context);
    });
  }

  // void _getCitiesList() {
  //   setState(() {
  //     ContactServices().loadJson().then((res) async => {

  //      });
  //   });
  // }
  _getDistrictList() {
    setState(() {
      ContactServices()
          .districtJson()
          .then((res) async => {print('distr$res'), districtList = res});
    });
  }

  _getTalukList(selDistrict) {
    setState(() {
      ContactServices()
          .talukJson(selDistrict)
          .then((res) async => {talukList = res});
    });
  }

  _getStateList() {
    setState(() {
      ContactServices().stateJson().then((res) => {stateList = res});
    });
  }

  void _getVillageList(selTaluk) {
    setState(() {
      ContactServices().villageJson(selTaluk).then(
          (res) async => {villageList = res, print('villageList$villageList')});
    });
  }
}
