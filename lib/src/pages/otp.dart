import 'package:flutter/material.dart';
import 'package:fluttergovt/src/pages/list_view.dart';

import 'package:fluttergovt/src/services/user_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Otp extends StatefulWidget {
  const Otp({Key? key}) : super(key: key);
  static const routeName = '/otp';
  @override
  _OtpState createState() => _OtpState();
}

class _OtpState extends State<Otp> {
  var _user = {"id": "", "otp": ""};
  bool loader = false;
  @override
  void initState() {
    super.initState();
    // _user = _user.initialize();
  }

  final scaffoldkey = GlobalKey<ScaffoldState>();
  final formkey = GlobalKey<FormState>();
  void _submit() {
    final form = formkey.currentState;
    if (form!.validate()) {
      form.save();
      performLogin();
    }
  }

  Future<void> performLogin() async {
    // Loader().openLoadingDialog(context);
    // Navigator.pushReplacementNamed(context, '/home');
    // setState(() {
    //   loader = true;
    // });
    final prefs = await SharedPreferences.getInstance();
    _user['id'] = prefs.getString('userId')!;
    UserServices logser = UserServices();
    var result = logser.otp(_user);
    result.then((u) async {
      Navigator.pop(context);
      // setState(() {
      //   loader = true;
      // });
      if (u['code'] == 'SUCCESS') {
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('token', u['body']['token']);
        print(u['body']['token']);
        Navigator.restorablePushNamed(context, ListViewPage.routeName);
        // Navigator.popAndPushNamed(context, '/home');
      } else {
        final snackbar = SnackBar(
          content: Text(u['message']),
        );

        ScaffoldMessenger.of(context).showSnackBar(snackbar);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final ButtonStyle style = ElevatedButton.styleFrom(
        textStyle: const TextStyle(fontSize: 20, color: Color(0xff476cfb)));
    return Scaffold(
      key: scaffoldkey,
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // (loader == true) ? Loader() : Text(""),
            const Text(
              "OTP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(143, 148, 251, .2),
                              blurRadius: 20.0,
                              offset: Offset(0, 10))
                        ]),
                    child: Form(
                        key: formkey,
                        // child: Column(
                        //   children: <Widget>[
                        // Container(
                        //   padding: EdgeInsets.all(8.0),
                        //   decoration: BoxDecoration(
                        //       border: Border(
                        //           bottom:
                        //               BorderSide(color: Colors.grey[100]))),
                        //   child: TextFormField(
                        //     decoration: InputDecoration(
                        //         border: InputBorder.none,
                        //         hintText: "Email Id",
                        //         hintStyle:
                        //             TextStyle(color: Colors.grey[400])),
                        //     // validator: (val) => emailValidator(val),
                        //     onSaved: (val) => _user.id = val,
                        //   ),
                        // ),
                        child: Container(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "OTP",
                                hintStyle: TextStyle(color: Colors.grey[400])),
                            // validator: (val) => val!=="",
                            onSaved: (val) => _user['otp'] = val!,
                            obscureText: true,
                          ),
                        )
                        //   ],
                        // ),
                        ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
            ElevatedButton(
              style: style,
              onPressed: () => {_submit()},
              child: Text(
                'submit',
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
            ),
            // RoundedButton(
            //   text: "Submit",
            //   press: () {
            //     _submit();
            //   },
            // ),
            // SizedBox(height: size.height * 0.03),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: <Widget>[
            //     Text(
            //       "Don’t have an Account ? ",
            //       style: TextStyle(color: kPrimaryColor),
            //     ),
            //     GestureDetector(
            //       onTap: () {
            //         Navigator.push(
            //           context,
            //           MaterialPageRoute(
            //             builder: (context) {
            //               return SignUpScreen();
            //             },
            //           ),
            //         );
            //       },
            //       child: Text(
            //         "Sign Up",
            //         style: TextStyle(
            //           color: kPrimaryColor,
            //           fontWeight: FontWeight.bold,
            //         ),
            //       ),
            //     )
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}
