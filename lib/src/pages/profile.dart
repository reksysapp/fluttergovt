import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttergovt/src/models/user_model.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final ImagePicker _picker = ImagePicker();
  var _user = UserModel();
  final scaffoldkey = GlobalKey<ScaffoldState>();
  final formkey = GlobalKey<FormState>();
  // ignore: prefer_typing_uninitialized_variables
  var _image;
  var _userId;
  var _serviceCall;
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: scaffoldkey,
        body: ListView(
          padding: const EdgeInsets.all(10.0),
          children: <Widget>[
            Form(
              key: formkey,
              child: Column(
                children: <Widget>[
                  Align(
                    child: CircleAvatar(
                      radius: 100,
                      backgroundColor: Color(0xff476cfb),
                      child: ClipOval(
                        child: SizedBox(
                          width: 180.0,
                          height: 180.0,
                          child: (_image != null)
                              ? Image.file(File(_image!.path))
                              // Image.file(
                              //     _image,
                              //     fit: BoxFit.fill,
                              //   )
                              : Image.network(
                                  "https://images.unsplash.com/photo-1502164980785-f8aa41d53611?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                                  fit: BoxFit.fill,
                                ),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: const Icon(
                          FontAwesomeIcons.fileImage,
                          // color: Color(0xff476cfb),
                        ),
                        onTap: () {
                          getGalleryImage();
                        },
                      ),
                      const SizedBox(width: 10),
                      GestureDetector(
                        child: const Icon(
                          FontAwesomeIcons.cameraRetro,
                          // color: Color(0xff476cfb),
                        ),
                        onTap: () {
                          getCameraImage();
                        },
                      ),
                    ],
                  ),
                  // Align(
                  //   alignment: Alignment.topCenter,
                  //   child: GestureDetector(
                  //     child: const Icon(
                  //       FontAwesomeIcons.fileImage,
                  //       // color: Color(0xff476cfb),
                  //     ),
                  //     onTap: () {
                  //       getImage();
                  //     },
                  //   ),
                  // ),
                  TextFormField(
                    decoration: const InputDecoration(
                      hintText: "Enter your name",
                      labelText: "Name",
                    ),
                    onSaved: (val) => {},
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      hintText: "Enter Phone Number",
                      labelText: "phone",
                    ),
                    onSaved: (val) => {},
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      hintText: "Enter Address",
                      labelText: "Delivery Address",
                    ),
                    onSaved: (val) => {},
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      hintText: "Enter Studied at",
                      labelText: "EmailId",
                    ),
                    onSaved: (val) => {},
                  ),
                  const SizedBox(
                    height: 50.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        color: const Color(0xff476cfb),
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/profile');
                        },
                        elevation: 4.0,
                        splashColor: Colors.blueGrey,
                        child: const Text(
                          'Cancel',
                          style: TextStyle(color: Colors.white, fontSize: 16.0),
                        ),
                      ),
                      RaisedButton(
                        color: const Color(0xff476cfb),
                        onPressed: () {
                          _submit(context);
                        },
                        elevation: 4.0,
                        splashColor: Colors.blueGrey,
                        child: const Text(
                          'Submit',
                          style: TextStyle(color: Colors.white, fontSize: 16.0),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future getGalleryImage() async {
    //  final XFile? image = await _picker.pickImage(source: ImageSource.camera);
    var image = await _picker.pickImage(source: ImageSource.gallery);
    print(image);
    setState(() {
      _image = image;
    });
  }

  Future getCameraImage() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    // final XFile? image = await _picker.pickImage(source: ImageSource.camera);
    var image = await _picker.pickImage(source: ImageSource.camera);
    print(image);

    setState(() {
      // _image = File(image!.path);
    });
  }

  Future _submit(BuildContext context) async {
    final form = formkey.currentState;

    if (form!.validate()) {
      form.save();

      // _serviceCall = ProfileServices().updateProfile((_user), _image);

      // _serviceCall.then((u) {
      //   Navigator.pushReplacementNamed(context, '/profile');
      //   final snackbar = new SnackBar(
      //     content: new Text(u['message']),
      //   );
      //   scaffoldkey.currentState.showSnackBar(snackbar);
      // });
    }
  }

  void saveProfile() {
    // final SnackBar snackbar = new SnackBar(
    //   content: new Text("profile updated"),
    // );
    // scaffoldkey.currentState.showSnackBar(snackbar);
  }
}
